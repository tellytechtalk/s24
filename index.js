
// console.log('Hello World')


// Exponent Operator 
const getCube = 2**3;
console.log( `The cube of 2 is ${getCube}`);


const address = [258, "Washington Ave NW", "California", 90011];
const [street_number, street_name, state, zipCode] = address;
console.log(`I live at  ${street_number} ${street_name} ${state} ${zipCode}`);


// Object Destructuring 

let animal = {
	animalName: "Kumi",
	animalType: "persian siamese",
	weight: 3,
	feet: 1,
	inches: 8,
};

console.log(`${animal.animalName} was a ${animal.animalType}. He weighed at ${animal.weight} kgs with a measurement of ${animal.feet} ft ${animal.inches} in.`);



// Array of Numbers

let arrayOfNumbers = [12, 24, 36, 48, 60, 72];
let [ num1, num2, num3, num4, num5, num6] = arrayOfNumbers;


// Looping with arrow Function 

/*randomNumbers.forEach(function(num){
	console.log(num);
})
*/

arrayOfNumbers.forEach((num) => {
	console.log(`${num}`)
})

// Reduce Array Method 

const initialValue = 0;
const sumWithInitial = arrayOfNumbers.reduce(
  (previousValue, currentValue) => previousValue + currentValue,
  initialValue
);

console.log(sumWithInitial);


// 